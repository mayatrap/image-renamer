﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Image_Renamer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter source directory");
            string sourceDir = Console.ReadLine();
            string targetDir = sourceDir + @"\renamed\";

            DirectoryInfo dir = new DirectoryInfo(sourceDir);
            FileInfo[] fileInfos = dir.GetFiles();

            Console.WriteLine("Enter number of the last image:");
            int lastNumber = int.Parse(Console.ReadLine()) + 1;

            int counter = 0;
            foreach (FileInfo fileInfo in fileInfos.OrderBy(f => f.CreationTime))
            {
                //Console.WriteLine($"Renaming file {fileInfo.Name} to image{lastNumber}{fileInfo.Extension}");
                File.Copy(fileInfo.FullName, targetDir + $"image{lastNumber}{fileInfo.Extension}", true);
                lastNumber++;
                counter++;
            }

            Console.WriteLine("DONE!");
            Console.WriteLine($"Processed {counter} images.");
            Console.WriteLine($"Files are in {targetDir}");
        }
    }
}