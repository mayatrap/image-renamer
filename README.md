# Image Renamer

Program that renames files (images)  
Example: fsdafsa.jpg -> image1.jpg  
  
Takes source directory and number of the last renamed images, then renames all files from that directory to image<number>.jpg  
The renamed files are put in a new directory called "renamed"  